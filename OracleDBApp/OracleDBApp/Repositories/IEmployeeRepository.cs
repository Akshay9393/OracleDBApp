﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OracleDBApp.Repositories
{
    interface IEmployeeRepository
    {
        object GetEmployeeList();

        object GetEmployeeDetails(int empId);

    }
}
